#!/usr/bin/env python3

from logbook import app
import sys

port = 5001
if len(sys.argv) == 2:
    port = sys.argv[1]

app.run(host="0.0.0.0", debug=True, port=port)

