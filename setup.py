import os
import pathlib
import re
from setuptools import setup, find_packages

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# Name of the module
MODULE_NAME = 'logbook'

# Dependencies for the module itself
DEPENDENCIES = ['flask',
                'flask-login',
                'flask-caching',
                'jinja2',
                'bs4',
                'lxml',
                'gunicorn',
                'flask-script',
                'flask-caching',
                'requests']

# Test dependencies that should only be installed for test purposes
TEST_DEPENDENCIES = ['pytest>=5.2',
                     'pytest-cov>=2.6',
                     'requests-mock',
                     ]

# pytest-runner to be able to run pytest via setuptools
SETUP_REQUIRES = ['pytest-runner']

# Extra dependencies for tools
EXTRA_DEPENDENCIES = {'doc': ['sphinx',
                              'travis-sphinx',
                              'sphinx_rtd_theme'],
                      'tools': ['mysql']
                      }

# The text of the README file
README = (HERE / "README.md").read_text()


def get_version():
    """ Reads package version number from package's __init__.py. """
    with open(os.path.join(
        os.path.dirname(__file__), MODULE_NAME, '__init__.py'
    )) as init:
        for line in init.readlines():
            res = re.match(r'^__version__ = [\'"](.*)[\'"]$', line)
            if res:
                return res.group(1)


# This call to setup() does all the work
setup(
    name=MODULE_NAME.replace('_', '-'),
    version=get_version(),
    description="A Paragliding Logbook",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/hatrix/paragliding_v2/",
    author_email="mael@legarrec.org",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
    ],
    packages=find_packages(include=('logbook',)),
    install_requires=DEPENDENCIES,
    tests_require=DEPENDENCIES + TEST_DEPENDENCIES,
    extras_require=EXTRA_DEPENDENCIES,
    setup_requires=SETUP_REQUIRES
)

