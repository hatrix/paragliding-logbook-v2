#!/bin/bash

if [ "$1" == "prod" ]; then
  if [ "$2" != "" ]; then
    gunicorn logbook:app -b 127.0.0.1:$2
  else
    gunicorn logbook:app -b 127.0.0.1:5001
  fi
elif [ "$1" == "debug" ]; then
  . venv/bin/activate
  python3 run_debug.py $2
else
  echo "Usage:"
  echo "  ./run.sh prod|debug"
fi

