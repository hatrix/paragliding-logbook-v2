import json

from logbook.views import app, config
from logbook.helpers import get_videos, clean_data, get_stats
from flask import render_template

@app.route('/stats')
def stats():
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)
    
    # Order the flights by number
    flights_stats = get_stats(data)

    return render_template('stats.html', stats=flights_stats)
