import json

from logbook.views import app, config
from logbook.helpers import get_videos, clean_data
from flask import render_template

@app.route('/')
@app.route('/index')
def index():
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)
    
    # Order the flights by number
    #data = get_videos(data, keep_non_video=True)
    data = clean_data(data)
    data = sorted(data, key=lambda f: int(f['number']), reverse=True)

    return render_template('index.html', flights=data)
