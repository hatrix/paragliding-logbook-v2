import json

from logbook.views import app, config
from logbook.helpers import get_licenses
from flask import render_template

@app.route('/licenses')
def licenses():
    # Get the licenses
    data = get_licenses()
    return render_template('licenses.html', licenses=data)
