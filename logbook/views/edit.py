import json
import copy
from collections import Counter
from flask_login import login_required
from flask import request, redirect, url_for

from logbook.views import app, config
from logbook.helpers import get_videos, clean_data
from logbook import render_template


def edit_json(flight_id, form):
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)

    # Create our new flight object
    flight = {}
    flight['number'] = int(form.get('number'))
    flight['duration'] = int(form.get('duration'))
    flight['elevation'] = int(form.get('elevation'))
    flight['date'] = form.get('date')
    flight['canopy'] = form.get('canopy')
    flight['landing'] = form.get('landing')
    flight['location'] = form.get('location')
    flight['take_off'] = form.get('take_off')
    flight['notes'] = form.get('notes').replace("\r\n", "\n")
    
    flight['gpx'] = form.get('tracks').split()
    flight['youtube'] = form.get('youtube').split()

    for key in flight.keys():
        if flight[key] == 'None':
            flight[key] = None

    # Replace the old one
    for i in range(len(data)):
        if data[i]['number'] == flight_id:
            data[i] = flight

    with open(config.Config.FLIGHTS, 'w') as flights:
        json.dump(sorted(data, key=lambda x: int(x['number'])),
                         flights,
                         sort_keys=True,
                         ensure_ascii=False,
                         indent=4)

    return redirect(url_for('edit_page'))


def add_youtube_link(data):
    for flight in data:
        if 'youtube' in flight.keys():
            flight['youtube_link']  = copy.deepcopy(flight['youtube'])
    return data


def show_edit_page():
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)

    # Order the flights by number
    data = add_youtube_link(data)
    data = get_videos(data, keep_non_video=True)
    data = clean_data(data)
    data = sorted(data, key=lambda f: int(f['number']), reverse=True)

    return render_template('edit.html', flights=data)


@app.route('/edit', methods=['GET'])
@login_required
def edit_page():
    return show_edit_page()


@app.route('/edit/<int:flight_id>', methods=['POST'])
@login_required
def edit_flight(flight_id):
    return edit_json(flight_id, request.form)
