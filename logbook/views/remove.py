import json
from collections import Counter
from flask_login import login_required
from flask import request, redirect, url_for

from logbook.views import app, config
from logbook.helpers import get_videos, clean_data
from logbook import render_template


def remove_flight(number):
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)

    final_data = []
    for flight in data:
        if int(flight['number']) == number:
            continue
        final_data.append(flight)

    with open(config.Config.FLIGHTS, 'w') as flights:
        json.dump(sorted(final_data, key=lambda x: int(x['number'])),
                         flights,
                         sort_keys=True,
                         ensure_ascii=False,
                         indent=4)

    return redirect(url_for('edit_page'))


@app.route('/remove/<int:flight_id>', methods=['GET'])
@login_required
def remove(flight_id):
    return remove_flight(flight_id)
