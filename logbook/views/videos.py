import json

from logbook.views import app, config
from logbook.helpers import get_videos, clean_data
from flask import render_template


@app.route('/videos')
def videos():
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)
    
    # Order the flights by number and embed the youtube videos
    data = get_videos(data)
    data = clean_data(data)
    data = sorted(data, key=lambda f: int(f['number']), reverse=True)

    return render_template('videos.html', flights=data)
