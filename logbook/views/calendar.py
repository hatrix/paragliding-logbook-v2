import json

from logbook.views import app, config
from logbook.helpers import clean_data, get_calendar
from logbook import render_template

@app.route('/calendar')
def calendar():
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)
    
    # Order the flights by number
    data = clean_data(data)
    cal = get_calendar(data)[::-1]

    return render_template('calendar.html', years=cal)
