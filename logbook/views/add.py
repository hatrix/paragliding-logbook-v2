import json
from collections import Counter
from flask_login import login_required
from flask import request, redirect, url_for

from logbook.views import app, config
from logbook.helpers import get_videos, clean_data
from logbook import render_template


def get_common(field, data):
    if data:
        return sorted(Counter(j[field] for j in data).items(),
                      key=lambda x: x[1])[-1][0]
    else:
        return None

def get_common_canopy(data):
    canopies = {}
    for flight in data:
        if flight['canopy'] not in canopies:
            canopies[flight['canopy']] = 0
        canopies[flight['canopy']] += int(flight['duration'])

    max_flown = max(canopies.items(), key=lambda x: x[1])
    return max_flown[0]


def show_add_form():
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)

    # Order the flights by number
    data = get_videos(data, keep_non_video=True)
    data = clean_data(data)
    data = sorted(data, key=lambda f: int(f['number']), reverse=True)

    # Get common fields to ease everything
    fields = ['landing', 'location', 'take_off']
    common_fields = {f: get_common(f, data) for f in fields}
    common_fields['canopy'] = get_common_canopy(data)
    common_fields['number'] = int(data[0]['number']) + 1

    # Get all the already recorded fields so it's easier to pick
    all_fields = {f: set([d[f] for d in data])  for f in fields + ['canopy']}
    print(all_fields)
    
    return render_template('add.html', all_fields=all_fields, **common_fields)


def add_to_json(form):
    with open(config.Config.FLIGHTS, 'r') as flights:
        data = json.load(flights)

    flight = {}
    flight['number'] = int(form.get('number'))
    flight['duration'] = int(form.get('duration'))
    flight['elevation'] = int(form.get('elevation'))
    flight['date'] = form.get('date')
    flight['canopy'] = form.get('canopy')
    flight['landing'] = form.get('landing')
    flight['location'] = form.get('location')
    flight['take_off'] = form.get('take_off')
    flight['notes'] = form.get('notes').replace("\r\n", "\n")
    
    flight['gpx'] = form.get('tracks').split() if form.get('tracks') else None
    flight['youtube'] = form.get('youtube').split() if form.get('youtube') else None

    data.append(flight)
    with open(config.Config.FLIGHTS, 'w') as flights:
        json.dump(sorted(data, key=lambda x: int(x['number'])),
                         flights,
                         sort_keys=True,
                         ensure_ascii=False,
                         indent=4)

    return redirect(url_for('index'))


@app.route('/add', methods=['GET', 'POST'])
@login_required
def add_flight():
    if request.method == 'GET':
        return show_add_form()
    elif request.method == 'POST':
        return add_to_json(request.form)
