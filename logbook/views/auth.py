import json

from flask import redirect, url_for, request
from flask_login import login_user, login_manager, UserMixin, login_required, logout_user, current_user

from logbook.models import User
from logbook.views import app, config
from logbook.helpers import get_videos, clean_data
from logbook import render_template


@app.route('/login', methods=['GET'])
def login_form():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    return render_template("login.html")


@app.route('/login', methods=['POST'])
def login():
    # code to validate and add user to database goes here
    name = request.form.get('username')
    password = request.form.get('password')
    remember = request.form.get('remember')

    user = None
    for us in app.users:
        if us.name == name and us.hash == password:
            user = us
            break

    if user:
        login_user(user, remember=remember)
        return redirect(url_for('index'))
    else:
        return redirect(url_for('err_403'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))
