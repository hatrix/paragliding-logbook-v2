from flask import (
    render_template,
    Blueprint,
    g,
    redirect,
    request,
    current_app,
    abort,
    url_for
)
from os import path
import locale
from logbook import app, config

# Definition of the tempalte to be used
template = path.join('logbook', 'templates', config.Config.ACTIVE_TEMPLATE)
site = Blueprint('site',
                 __name__,
                 template_folder=template,
                 url_prefix='/')

# Import views
from logbook.views.index import index
from logbook.views.videos import videos
from logbook.views.stats import stats
from logbook.views.calendar import calendar
from logbook.views.licenses import licenses

from logbook.views.errors import err_403, err_404

# Requires login
from logbook.views.auth import login, login_form, logout
from logbook.views.add import add_flight
from logbook.views.edit import edit_page, edit_flight
from logbook.views.remove import remove
