import json

from logbook.views import app, config
from logbook.helpers import get_videos, clean_data, get_stats
from flask import render_template

@app.route('/403')
def err_403():
    return '403'

@app.route('/404')
def err_404():
    return '404'
