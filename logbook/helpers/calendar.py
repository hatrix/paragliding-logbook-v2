import calendar 
import bs4 as bs

def get_calendar(data):
    dates = {}
    for flight in data:
        if flight['date'] not in dates:
            dates[flight['date']] = 1
        else:
            dates[flight['date']] += 1

    days = [(word[:3], word[0]) for word in calendar.day_name]
    different_years = sorted(set([int(y.split('-')[0]) for y in dates]),
                             reverse=True)

    number_flights_per_year = { year: 0 for year in different_years }
    number_flights_per_month = {}

    for date, number in dates.items():
        number_flights_per_year[int(date.split('-')[0])] += number

        if date[:-3] not in number_flights_per_month.keys():
            number_flights_per_month[date[:-3]] = 0
        number_flights_per_month[date[:-3]] += number
    
    result = []
    # Loop through every year
    for year in different_years:
        c = calendar.HTMLCalendar(calendar.MONDAY)
        html = c.formatyear(year)
        html = bs.BeautifulSoup(html, 'lxml') # html of year
        
        # Replace day names by their first letter
        for day, letter in days:
            texts = html.find_all(text=day)
            for t in texts:
                new_text = str(t).replace(day, letter)
                t.replace_with(new_text)

        # Add total number of flights under year's number
        # fuck, that's hard to do with this library
        br = html.new_tag('br')
        year_td = html.find(text=year).parent
        
        year_td.string = ''
        year_td.append(bs.NavigableString(str(year)))
        year_td.append(br)

        nb_j = str(number_flights_per_year[year]) + ' flights'
        year_td.append(bs.NavigableString(nb_j))
        
        # Add color for day containing a flight
        for date in dates.keys():
            y = int(date.split('-')[0])
            m = int(date.split('-')[1])
            d = int(date.split('-')[2])

            if y != year:
                continue
            
            html_month = html.find(text=calendar.month_name[m])\
                             .parent.parent.parent
            flight_day = html_month.find(text=d).parent
        
            # Add number of flights
            month_tag = html.find('th', {'class': 'month'}, text=calendar.month_name[m])
            if month_tag:
                br = html.new_tag('br')
                month_name = month_tag.text
                month_tag.string = ''
                month_tag.append(bs.NavigableString(month_name))
                month_tag.append(br)
                nb_j = str(number_flights_per_month[date[:-3]]) + ' flights'
                month_tag.append(bs.NavigableString(nb_j))
            
            if dates[date] >= 4:
                color = 'red'
            elif dates[date] >= 3:
                color = 'orange'
            elif dates[date] == 2:
                color = 'yellow'
            else: # one flight
                color = 'green'
            flight_day['class'] += ['color-' + color]

        result.append(html)

    return result
