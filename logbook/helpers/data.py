import copy
import re
from time import strptime, strftime 

def get_videos(data, keep_non_video=False):
    flights_video = copy.deepcopy(data)

    if not keep_non_video:
        flights_video = [j for j in flights_video if 'youtube' in j.keys()]

    for j in flights_video:
        if 'youtube' in j.keys() and j['youtube']:
            for i in range(len(j['youtube'])):
                reg = r"http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\\" \
                      r".be\/)([\w\-\_]*)(&(amp;)?<200b>[\w\?<200b>=]*)?"
                sub = r'<div class="video_sup">' \
                      r'<div class="video_container">' \
                      r'<iframe class="video"' \
                      r'allowfullscreen="allowfullscreen"' \
                      r'src="https://www.youtube.com/embed/\1"></iframe>' \
                      r'</div>' \
                      r'</div>'
                j['youtube'][i] = re.sub(reg, sub, j['youtube'][i])

    return flights_video


def clean_data(data):
    flights = []
    for flight in data:
        f = copy.deepcopy(flight)

        try:
            date = strptime(f['date'], '%d%m%y')
        except:
            date = strptime(f['date'], '%Y-%m-%d')
        f['date'] = strftime('%Y-%m-%d', date)

        # Fix the gpx tracks
        if 'gpx' in f.keys() and f['gpx']:
            if len(f['gpx']) == 1 and f['gpx'][0] == '':
                f['gpx'] = None
        else:
            f['gpx'] = None

        # Fix the youtube links
        if 'youtube' in f.keys() and f['youtube']:
            if len(f['youtube']) == 1 and f['youtube'][0] == '':
                f['youtube'] = None
        else:
            f['youtube'] = None

        flights.append(f)

    return flights

    
