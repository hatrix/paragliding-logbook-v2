import json
from time import strptime, strftime
from logbook import config

def get_licenses():                                                      
    with open(config.Config.LICENCES, 'r') as fp:                                              
        try:                                                                    
            licenses = json.load(fp)                                            
        except:                                                                 
            licenses = []                                                       
    
    for l in licenses:                                                          
        date = strptime(l['date'], '%d%m%y')                                    
        l['date'] = strftime('%Y-%m-%d', date)                                  
                                                                                
    return sorted(licenses, key=lambda x: x['date'])    
