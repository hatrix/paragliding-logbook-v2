from collections import Counter  
from logbook import config


def get_stats(data, display=False):
    def get_numbers(field, data):
        field_num = sorted(Counter(j[field] for j in data).items(), 
                    key=lambda x: x[0], reverse=True)

        return field_num

    def get_total_flight_time(data):
        res = [sum([int(f['duration']) for f in data]), 0]
        res[1] = "{:02}".format(res[0] % 60)
        res[0] = res[0] // 60
        return res

    def get_flight_time_for_field(data, field):
        res = {}
        for flight in data:
            if field not in res:
                res[field] = {}

            if flight[field] not in res[field]:
                res[field][flight[field]] = 0

            res[field][flight[field]] += int(flight['duration'])
        return res

    fields = ['location', 'take_off', 'landing', 'canopy']

    res = {}
    times = {}
    for field in fields:
        numbers = get_numbers(field, data)
        res[field] = numbers

        times.update(get_flight_time_for_field(data, field))

        # Debug
        if display:
            print(field.title())
            print('-' * 24)
            for value, count in numbers:
                print('  {:10s} {} flights'.format(value, count))
            print('')


    # Add the duration for each field
    new_data = {}
    for field, values in res.items():
        new_data[field] = []
        for item in values:
            formatted_duration = [0, "{:02}".format(times[field][item[0]] % 60)]
            formatted_duration[0] = times[field][item[0]] // 60
            new_data[field].append((item[0], item[1], formatted_duration))

    # Hours, minutes
    new_data['flight_time'] = get_total_flight_time(data)

    return new_data
