from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, username, hash):
        self.name = username
        self.hash = hash

    def get_id(self):
        return self.name
