import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    BABEL_DEFAULT_LOCALE = 'en'
    LANGUAGES = ['en', 'fr', 'de']
    ACTIVE_TEMPLATE = 'logbook-default'
    FLIGHTS = os.path.join(os.path.dirname(__file__), 'resources', 'flights.json')
    LICENCES = os.path.join(os.path.dirname(__file__), 'resources', 'licenses.json')

    users = [('CHANGEME', 'CHANGEME')]
