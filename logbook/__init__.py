from flask import Flask, request, g, redirect, url_for, send_from_directory
from flask_caching import Cache
from flask import render_template as rt
from flask_login import current_user

from datetime import datetime
import os
from werkzeug.routing import BaseConverter, ValidationError
from flask_login import LoginManager

from . import config
from logbook.models import User

__version__ = '0.1'

# ---------------------------------------

def render_template(*args, **kwargs):
    return rt(args, **kwargs, current_user=current_user)
    

def create_static_users():
    users = []
    for username, password in config.Config.users:
        users.append(User(username, password))

    return users


def create_app():
    # setup app
    template_dir = os.path.dirname(__file__)
    template_dir = os.path.join(template_dir, 'templates')
    template_dir = os.path.join(template_dir, config.Config.ACTIVE_TEMPLATE)

    app = Flask(__name__, static_url_path='', template_folder=template_dir)
    app.config.from_object(config.Config)

    app.users = create_static_users()

    app.config['SECRET_KEY'] = 'une-super-secret-key-mdr-45'
    login_manager = LoginManager()
    login_manager.login_view = 'login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(username):
        for user in app.users:
            if user.name == username:
                return user
        return None

    return app


app = create_app()
cache = Cache(app,config={'CACHE_TYPE': 'simple'})

# Import views
from logbook.views import index

# Define default route when loading the website
@app.route('/')
def home():
    return url_for(index)


# Define static routes
@app.route('/js/<path:path>')
def send_js(path):
    p = os.path.join('static', config.Config.ACTIVE_TEMPLATE, 'js')
    return send_from_directory(p, path)

@app.route('/css/<path:path>')
def send_css(path):
    p = os.path.join('static', config.Config.ACTIVE_TEMPLATE, 'css')
    return send_from_directory(p, path)

@app.route('/igc/<path:path>')
def send_igc(path):
    p = os.path.join('static', 'igc')
    return send_from_directory(p, path)

@app.route('/cesium/<path:path>')
def send_cesium(path):
    p = os.path.join('static', 'cesium')
    return send_from_directory(p, path)
