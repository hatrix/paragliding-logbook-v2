window.CESIUM_BASE_URL = '/cesium/';

function igcToGpx(filePath) {
  // Converts an IGC file to a parsed GPX Document
  // Open the IGC file
  const request = new XMLHttpRequest();
  request.open("GET", filePath, false);
  request.send(null);
  const igc_text = request.responseText.split('\n');

  // Create the GPX
  let gpx_string = '';
  gpx_string += '<?xml version="1.0" encoding="UTF-8"?>\n';
  gpx_string += '<gpx>\n';
  gpx_string += '<trk>\n';
  gpx_string += '<name>test</name>\n';
  gpx_string += '<trkseg>\n';

  let day;
  igc_text.forEach(line => {
      if (line.startsWith('HFDTEDATE')) {  // date
  day = line.split(':')[1].split(',')[0];
      }

      if (line[0] === 'B') {  // B records are positions
  let time = line.substring(1, 7);
  let latitude = line.substring(7, 15);
  let longitude = line.substring(15, 24);
  let fix = line.substring(24, 25);
  let altitude_pressure = line.substring(25, 30);
  let altitude_gnss = line.substring(30, 35);

  // Rework the data
  // DD MM mmm: degrees, minutes, decimal minutes
  latitude = (parseInt(latitude.substring(0, 2)) + (parseInt(latitude.substring(2, 4)) + parseInt(latitude.substring(4, 7)) / 1000) / 60).toFixed(6);
  longitude = (parseInt(longitude.substring(0, 3)) + (parseInt(longitude.substring(3, 5)) + parseInt(longitude.substring(5, 8)) / 1000) / 60).toFixed(6);
  altitude_gnss = parseFloat(altitude_gnss) + 50;

  // Construct ISO 8601 date string
  const dayPart = "20" + `${day.substring(4, 6)}-${day.substring(2, 4)}-${day.substring(0, 2)}`;
  const timePart = `${time.substring(0, 2)}:${time.substring(2, 4)}:${time.substring(4, 6)}`;
  const dateTimeString = `${dayPart}T${timePart}Z`;
  const timeFormatted = new Date(dateTimeString).toISOString();

  gpx_string += `<trkpt lat="${latitude}" lon="${longitude}">\n`;
  gpx_string += `  <ele>${altitude_gnss}</ele>\n`;
  gpx_string += `  <time>${timeFormatted}</time>\n`;
  gpx_string += `</trkpt>\n`;
      }
  });

  gpx_string += "</trkseg>";
  gpx_string += "</trk>";
  gpx_string += "</gpx>";

  var parser = new DOMParser();
  var gpx_data = parser.parseFromString(gpx_string, "text/xml");
  return gpx_data;
}
