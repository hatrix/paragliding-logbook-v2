function remove_flight(number)
{
  var ret = confirm("Are you sure you want to remove flight n°" + number + "?");

  if (ret == true)
    window.location.href = "/remove/" + number;
}
